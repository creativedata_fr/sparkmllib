package com.cloudera.utils

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.Vectors

object SaveSVM {
	def main(args: Array[String]) {
		val sc = new SparkContext(new SparkConf().setAppName("Spark SVM"))
		val data: RDD[LabeledPoint] = sc.textFile(args(0)).map(labelize(_,args(2).toInt,args(3)))
		MLUtils.saveAsLibSVMFile(data, args(1))
	}

	def labelize(line: String, target: Integer, separator: String): LabeledPoint =	{
		val arrayData = line.split(separator).map(_.toDouble)
        val withoutTarget = arrayData.take(target) ++ arrayData.drop(target + 1)
		LabeledPoint(arrayData.apply(target), Vectors.dense(withoutTarget))
	}
}
