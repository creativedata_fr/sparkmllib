/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cloudera.sparkmllib

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.util.MLUtils

object SparkLinearRegressionWithSGD {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("Spark LinearRegressionWithSGD"))

    val data = MLUtils.loadLibSVMFile(sc, args(0))

    val splits = data.randomSplit(Array(0.8, 0.2), seed = 11L)
    val training = splits(0).cache()
    val test = splits(1)

    // Run training algorithm to build the model
    val numIterations = args(1).toInt
   // val model = LinearRegressionWithSGD.setIntercept(true).train(training, numIterations,0.2)
	val algorithm = new LinearRegressionWithSGD()
	  algorithm.setIntercept(true)
	  algorithm.optimizer.setNumIterations(numIterations).setStepSize(args(2).toDouble)
	val model =  algorithm.run(training)
    // Compute raw scores on the test set.
    val scoreAndLabels: RDD[(Double, Double)] = test.map { point =>
      val score = model.predict(point.features)
      (score, point.label)
    }

    // Get evaluation metrics.
    val metrics = new RegressionMetrics(scoreAndLabels)
    scoreAndLabels.collect().foreach(println)
    println("meanAbsoluteError : " + metrics.meanAbsoluteError)
  }
}
