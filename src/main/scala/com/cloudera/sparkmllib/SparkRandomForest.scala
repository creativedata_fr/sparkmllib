

package com.cloudera.sparkmllib

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.tree.RandomForest

object SparkRandomForest {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("Spark LinearRegressionWithSGD"))

// Load and parse the data file.
val data = MLUtils.loadLibSVMFile(sc, args(0))
// Split the data into training and test sets (30% held out for testing)
val splits = data.randomSplit(Array(0.7, 0.3))
val (trainingData, testData) = (splits(0), splits(1))

// Train a RandomForest model.
//  Empty categoricalFeaturesInfo indicates all features are continuous.
val numClasses = args(1).toInt //2
val categoricalFeaturesInfo = Map[Int, Int]()
val numTrees = args(2).toInt //3 // Use more in practice.
val featureSubsetStrategy = "auto" // Let the algorithm choose.
val impurity = "variance"
val maxDepth = args(3).toInt //4
val maxBins = args(4).toInt //32

val model = RandomForest.trainRegressor(trainingData, categoricalFeaturesInfo,
  numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)

// Evaluate model on test instances and compute test error
val labelsAndPredictions = testData.map { point =>
  val prediction = model.predict(point.features)
  (point.label, prediction)
}
val testMSE = labelsAndPredictions.map{ case(v, p) => math.pow((v - p), 2)}.mean()
println("Test Mean Squared Error = " + testMSE)
println("Learned regression forest model:\n" + model.toDebugString)


    // Get evaluation metrics.
    val metrics = new RegressionMetrics(labelsAndPredictions)
    labelsAndPredictions.collect().foreach(println)
    println("meanAbsoluteError : " + metrics.meanAbsoluteError)
  }
}
