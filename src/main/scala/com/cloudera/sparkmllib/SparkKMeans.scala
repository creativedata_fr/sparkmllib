/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cloudera.sparkmllib

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.mllib.clustering.KMeans
import org.apache.spark.mllib.clustering.KMeansModel
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.rdd.RDD


object SparkKMeans {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setAppName("Spark KMeans"))

    val data: RDD[String] = sc.textFile("pivot.csv").cache()
    val parsedData: RDD[Vector] = data.map(s => Vectors.dense(s.split(';').tail.map(_.toDouble))).cache()
    
    val numClusters: Int = args(0).toInt
    val numIterations: Int = args(1).toInt
    val runs: Int = args(2).toInt
    val clusters: KMeansModel = KMeans.train(parsedData, numClusters, numIterations, runs)

    val WSSSE: Double = clusters.computeCost(parsedData)
    println("Within Set Sum of Squared Errors = " + WSSSE)
    
    clusters.clusterCenters.foreach(println)
    
    var predictData: Array[List[String]] = Array.fill[List[String]](numClusters)(Nil)
    data.collect().foreach(s => {
        val data = s.split(';')
        val cluster: Int = clusters.predict(Vectors.dense(data.tail.map(_.toDouble)))
        predictData(cluster) = data.head :: predictData(cluster)
    })
    
    predictData.foreach(println)
    
  }
}
